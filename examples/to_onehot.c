#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "kann.h"
#include "kann_extra/kann_data.h"

/*
void to_onehot(kann_data_t *data, int max_user_id, int max_item_id)
{
    size_t skip = max_user_id + max_item_id;
    size_t tot_sze = skip * data->n_row;
    float *tmp = (float*) calloc(tot_sze, sizeof(float));
    if (tmp == NULL) {
        fprintf(stderr, "Cannot allocate memory of size %d GB\n", tot_sze*4/1024/1024/1024);
        exit(1);
    }
    for(int row = 0; row < data->n_row; row++) {
        size_t user_id = (size_t)data->x[row][0];
        size_t item_id = (size_t)data->x[row][1];
        tmp[skip*row+user_id] = 1;
        tmp[skip*row+max_user_id+item_id] = 1;
        free(data->x[row]);
        data->x[row] = &tmp[skip*row];
    }
    data->n_col = max_user_id + max_item_id;
}
*/

void build_dok_matrix(kann_data_t *in, kann_dok_t *dok, int max_user_id)
{
    int n_row_in = in->n_row;
    dok->n_row = max_user_id;
    dok->idx = (int**) calloc(max_user_id, sizeof(int*));
    dok->sze = (int*) calloc(max_user_id, sizeof(int));
    assert(dok->idx != NULL);
    assert(dok->sze != NULL);

    int id = 0, cnt = 0, sum = 0;
    for (int i = 0; i < n_row_in; i++) {
        if ((int) in->x[i][0] > id) {
            dok->sze[id] = cnt;
            sum += cnt;
            cnt = 0;
            id += 1;
        } 
        cnt += 1;
    }
    dok->sze[id] = cnt;
    sum += cnt;
    //printf("id: %d sum: %d n_row_in: %d\n", id, sum, n_row_in);

    int *tmp = (int*) calloc(sum, sizeof(int));
    cnt = 0;
    for (int i = 0; i < max_user_id; i++) {
        dok->idx[i] = &tmp[cnt];
        cnt += dok->sze[i];
    }

    id = 0;
    cnt = 0;
    for (int i = 0; i < n_row_in; i++, cnt++) {
        int user_id = (int) in->x[i][0];
        int item_id = (int) in->x[i][1];
        if (user_id > id) {
            cnt = 0;
            id += 1;
        }
        dok->idx[user_id][cnt] = item_id;
    }
}

