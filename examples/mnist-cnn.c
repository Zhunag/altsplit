#include <unistd.h>
#include <stdlib.h>
#include <assert.h>
#include <mpi.h>
#include "kann_extra/kann_data.h"
#include "kann.h"

int main(int argc, char *argv[])
{
	kann_t *ann;
	kann_data_t *x, *y;
	char *fn_in = 0, *fn_out = 0;
	int c, mini_size = 64, max_epoch = 20, max_drop_streak = 10, seed = 131, n_h_fc = 128, n_h_flt = 32, n_threads = 1, n_layers = 3;
    int batches = 0;
	float lr = 0.001f, dropout = 0.2f, frac_val = 0.1f;

    int provided;
    MPI_Init_thread(&argc, (char ***)&argv, MPI_THREAD_MULTIPLE, &provided);
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

	while ((c = getopt(argc, argv, "i:o:m:h:f:d:s:t:v:l:r:b:B:")) >= 0) {
		if (c == 'i') fn_in = optarg;
		else if (c == 'o') fn_out = optarg;
		else if (c == 'm') max_epoch = atoi(optarg);
		else if (c == 'h') n_h_fc = atoi(optarg);
		else if (c == 'f') n_h_flt = atoi(optarg);
		else if (c == 'd') dropout = atof(optarg);
		else if (c == 's') seed = atoi(optarg);
		else if (c == 't') n_threads = atoi(optarg);
		else if (c == 'v') frac_val = atof(optarg);
		else if (c == 'l') n_layers = atoi(optarg);
		else if (c == 'r') lr = atof(optarg);
		else if (c == 'b') batches = atoi(optarg);
		else if (c == 'B') mini_size = atoi(optarg);
	}

	if (argc - optind == 0 || (argc - optind == 1 && fn_in == 0)) {
		FILE *fp = stdout;
		fprintf(fp, "Usage: mnist-cnn [-i model] [-o model] [-t nThreads] <x.knd> [y.knd]\n");
		fprintf(fp, "-i fn_in\n");
		fprintf(fp, "-o fn_out\n");
		fprintf(fp, "-m max_epoch\n");
		fprintf(fp, "-h num_neurons\n");
		fprintf(fp, "-f num_filters\n");
		fprintf(fp, "-d dropout\n");
		fprintf(fp, "-s seed\n");
		fprintf(fp, "-t n_threads\n");
		fprintf(fp, "-v frac_val\n");
		fprintf(fp, "-l n_layers\n");
		fprintf(fp, "-r learning rate\n");
		fprintf(fp, "-b batches\n");
		fprintf(fp, "-B mini_size\n");
		return 1;
	}

    fprintf(stdout, "%s %s %d %d %d %f %d %d %f %d %f %d %d\n", fn_in, fn_out, max_epoch, n_h_fc, n_h_flt, dropout, seed, n_threads, frac_val, n_layers, lr, batches, mini_size);
    fprintf(stdout, "%s %s\n", argv[optind], argv[optind+1]);
    kad_trap_fe();
    kann_drand_init(seed);
    kann_srand(seed);
    fprintf(stdout, "pre building\n");
    if (fn_in) {
        ann = kann_load(fn_in);
    } else {
        int flt = n_h_flt / size;
        kad_node_t *t;
        t = kad_feed(4, 1, 3, 32, 32), t->ext_flag |= KANN_F_IN;
        fprintf(stdout, "Model building0\n");
        //t = kad_fm_allgather2(kad_relu(kann_layer_conv2d_mpi(t, flt, 16, 16, 4, 4, 0, 0))); // 3x3 kernel; 1x1 stride; 0x0 padding
        t = kad_fm_allgather2(kad_relu(kann_layer_conv2d_mpi(t, flt, 3, 3, 1, 1, 0, 0))); // 3x3 kernel; 1x1 stride; 0x0 padding
        fprintf(stdout, "Model building1\n");
        t = kad_max2d(t, 2, 2, 2, 2, 0, 0); // 2x2 kernel; 2x2 stride; 0x0 padding
        //t = kann_layer_dropout(t, dropout);
        t = kad_fm_allgather2(kad_relu(kann_layer_conv2d_mpi(t, flt, 3, 3, 1, 1, 0, 0)));
        fprintf(stdout, "Model building2\n");
        t = kad_max2d(t, 2, 2, 2, 2, 0, 0); // 2x2 kernel; 2x2 stride; 0x0 padding
        t = kad_fm_allgather2(kad_relu(kann_layer_conv2d_mpi(t, flt, 3, 3, 1, 1, 0, 0))); 
        t = kad_max2d(t, 2, 2, 2, 2, 0, 0); 
        t = kad_fm_allgather(kad_relu(kann_layer_conv2d_mpi(t, flt, 3, 3, 1, 1, 0, 0))); 
        t = kad_max2d(t, 2, 2, 2, 2, 0, 0); 
        //t = kann_layer_dropout(t, dropout);
        //ann = kann_insert_alt_mlp_mpi(t, 10, KANN_C_CEM, n_layers, n_h_fc, dropout, size, mini_size, rank);
        ann = kann_new(kann_layer_cost_mpi(t, 10, KANN_C_CEM, ALTREDUCE_ONEWAY), 0);
        fprintf(stdout, "Model building3\n");
    }

    x = kann_data_read(argv[optind]);
    y = argc - optind >= 2? kann_data_read(argv[optind+1]) : 0;

    fprintf(stdout, "Model building\n");
	if (y) { // training
		assert(y->n_col == 10);
		if (n_threads > 1) kann_mt(ann, n_threads, mini_size);
		kann_train_fnn1_mpi(ann, lr, mini_size, max_epoch, max_drop_streak, frac_val, x->n_row, x->x, y->x, rank, batches);
		//kann_train_fnn1(ann, lr, mini_size, max_epoch, max_drop_streak, frac_val, x->n_row, x->x, y->x);
		if (fn_out) kann_save(fn_out, ann);
		kann_data_free(y);
	} else { // applying
		int i, j, n_out;
		kann_switch(ann, 0);
		n_out = kann_dim_out(ann);
		assert(n_out == 10);
		for (i = 0; i < x->n_row; ++i) {
			const float *y;
			y = kann_apply1(ann, x->x[i]);
			if (x->rname) printf("%s\t", x->rname[i]);
			for (j = 0; j < n_out; ++j) {
				if (j) putchar('\t');
				printf("%.3g", y[j] + 1.0f - 1.0f);
			}
			putchar('\n');
		}
	}

	kann_data_free(x);
	kann_delete(ann);
    MPI_Finalize();
	return 0;
}
