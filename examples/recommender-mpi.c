#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <stdio.h>
#include <mpi.h>
#include <time.h>
#include "kann.h"
#include "kann_extra/kann_data.h"

extern int REDUCE_LEN;
extern int REDUCE_LEN_OUT;

static kann_t *model_gen_plain(int n_in, int n_out, int loss_type, int n_h_layers, int n_h_neurons, float h_dropout, int max_user_id)
{
    int i;
    kad_node_t *t, *user_in, *item_in, *user_mat, *item_mat;
    t = kann_layer_input(n_in);
    for (i = 0; i < n_h_layers; ++i) {
        if ( i == 0 ) {
            user_in = kad_slice(t, 1, 0, max_user_id);
            item_in = kad_slice(t, 1, max_user_id, n_in);
            user_mat = kann_layer_dense_nobias(user_in, n_h_neurons/2);
            item_mat = kann_layer_dense_nobias(item_in, n_h_neurons/2);
            t = kad_concat(1, 2, user_mat, item_mat);
        } else {
            t = kad_relu(kann_layer_dense(t, n_h_neurons));
        }
        n_h_neurons /= 2;
    }
    return kann_new(kann_layer_cost(t, n_out, KANN_C_CEB), 0);
}

int main(int argc, char *argv[])
{
    int max_epoch = 100, mini_size = 64, max_drop_streak = 10, loss_type = KANN_C_CEB;
    int i, j, c, n_h_neurons = 240, n_h_layers = 1, seed = time(NULL), n_threads = 1;
    int split_mode = ALTREDUCE, batches = 0;
    size_t max_user_id = 6040, max_item_id = 3706; //MovieLens 1m
    int num_neg =4; int top_K = 10; 

    kann_data_t *in = 0;
    kann_data_t *val = 0;
    kann_t *ann = 0;
    char *out_fn = 0, *in_fn = 0;
    float lr = 0.001f, frac_val = 0.1f, h_dropout = 0.0f;

    int provided;
    MPI_Init_thread(&argc, (char ***)&argv, MPI_THREAD_MULTIPLE, &provided);
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    while ((c = getopt(argc, argv, "n:l:s:r:m:B:o:i:d:v:Mt:S:b:U:I:N:K:D")) >= 0) {
        if (c == 'n') n_h_neurons = atoi(optarg);
        else if (c == 'l') n_h_layers = atoi(optarg);
        else if (c == 's') seed = atoi(optarg);
        else if (c == 'i') in_fn = optarg;
        else if (c == 'o') out_fn = optarg;
        else if (c == 'r') lr = atof(optarg);
        else if (c == 'm') max_epoch = atoi(optarg);
        else if (c == 'B') mini_size = atoi(optarg);
        else if (c == 'd') h_dropout = atof(optarg);
        else if (c == 'v') frac_val = atof(optarg);
        else if (c == 'M') loss_type = KANN_C_CEM;
        else if (c == 't') n_threads = atoi(optarg);
        else if (c == 'S') split_mode = atoi(optarg);
        else if (c == 'b') batches = atoi(optarg);
        else if (c == 'U') max_user_id = atoi(optarg);
        else if (c == 'I') max_item_id = atoi(optarg);
        else if (c == 'N') num_neg = atoi(optarg);
        else if (c == 'K') top_K = atoi(optarg);
    }

    if (argc - optind < 1) {
        if (rank == 0) {
            FILE *fp = stdout;
            fprintf(fp, "Usage: mlp [options] <in.knd> [truth.knd]\n");
            fprintf(fp, "Options:\n");
            fprintf(fp, "  Model construction:\n");
            fprintf(fp, "    -i FILE     read trained model from FILE []\n");
            fprintf(fp, "    -o FILE     save trained model to FILE []\n");
            fprintf(fp, "    -s INT      random seed [%d]\n", seed);
            fprintf(fp, "    -l INT      number of hidden layers [%d]\n", n_h_layers);
            fprintf(fp, "    -n INT      number of hidden neurons per layer [%d]\n", n_h_neurons);
            fprintf(fp, "    -d FLOAT    dropout at the hidden layer(s) [%g]\n", h_dropout);
            fprintf(fp, "    -M          use multi-class cross-entropy (binary by default)\n");
            fprintf(fp, "  Model training:\n");
            fprintf(fp, "    -r FLOAT    learning rate [%g]\n", lr);
            fprintf(fp, "    -m INT      max number of epochs [%d]\n", max_epoch);
            fprintf(fp, "    -B INT      mini-batch size [%d]\n", mini_size);
            fprintf(fp, "    -v FLOAT    fraction of data used for validation [%g]\n", frac_val);
            fprintf(fp, "    -t INT      number of threads [%d]\n", n_threads);
            fprintf(fp, "    -S INT      Split mode [%d]\n", split_mode);
            fprintf(fp, "    -b INT      number of batches [%d]\n", batches);
            fprintf(fp, "    -U INT      Max user ID [%ld]\n", max_user_id);
            fprintf(fp, "    -I INT      Max item ID [%ld]\n", max_item_id);
            fprintf(fp, "    -N INT      Num of Negatives [%d]\n", num_neg);
            fprintf(fp, "    -K INT      TOP K [%d]\n", top_K);
        }
        return 0;
    }
    if (argc - optind == 1 && in_fn == 0) {
        if (rank == 0)
            fprintf(stderr, "ERROR: please specify a trained model with option '-i'.\n");
        return 0;
    }

    kad_trap_fe();

    MPI_Bcast(&seed, 1, MPI_INT, 0, MPI_COMM_WORLD);
    kann_drand_init(seed);
    kann_srand(seed);

    in = kann_data_read(argv[optind]);
    val = kann_data_read(argv[optind+1]);
    fprintf(stdout, "in->row %d in->col %d\n", in->n_row, in->n_col);
    fprintf(stdout, "val->row %d val->col %d\n", val->n_row, val->n_col);

    kann_dok_t *in_dok = (kann_dok_t*) malloc(sizeof(kann_dok_t));
    build_dok_matrix(in, in_dok, max_user_id);

    if (in_fn) {
        ann = kann_load(in_fn);
        assert(kann_dim_in(ann) == in->n_col);
    }

    if (optind+1 < argc) { // train
        //if (ann) assert(kann_dim_out(ann) == out->n_col);
        //else ann = model_gen(in->n_col, out->n_col, loss_type, n_h_layers, n_h_neurons, h_dropout, size, mini_size, rank, split_mode);
        ann = model_gen_plain(max_user_id+max_item_id, 1, loss_type, n_h_layers, n_h_neurons, h_dropout, max_user_id);
        if (n_threads > 1) kann_mt(ann, n_threads, mini_size);
        kann_train_fnn1_mpi_onehot(ann, lr, mini_size, max_epoch, max_drop_streak, frac_val, in->n_row, in->x, val->n_row, 
                val->n_col, val->x, rank, batches, max_user_id, max_item_id, in_dok, num_neg, top_K);
        if (out_fn && rank == 0) kann_save(out_fn, ann);
    } else { // apply
        int n_out;
        if (in->cname) {
            printf("#sample");
            for (i = 0; i < in->n_col; ++i)
                printf("\t%s", in->cname[i]);
            printf("\n");
        }
        kann_switch(ann, 0);
        n_out = kann_dim_out(ann);
        for (i = 0; i < in->n_row; ++i) {
            const float *y;
            y = kann_apply1(ann, in->x[i]);
            if (rank == 0) {
                if (in->rname) printf("%s\t", in->rname[i]);
                for (j = 0; j < n_out; ++j) {
                    if (j) putchar('\t');
                    printf("%.3g", y[j] + 1.0f - 1.0f);
                }
                putchar('\n');
            }
        }
    }

    kann_delete(ann);
    kann_data_free(in);
    kann_data_free(val);
    MPI_Finalize();
    return 0;
}
